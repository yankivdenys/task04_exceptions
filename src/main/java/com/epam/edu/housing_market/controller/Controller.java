package com.epam.edu.housing_market.controller;

import com.epam.edu.housing_market.model.Place;
import java.util.ArrayList;
import java.util.List;

import com.epam.edu.housing_market.model.Catalog;

public class Controller implements Controllable {

  private final Catalog catalog;

  public Controller(final Catalog catalog) {
    this.catalog = catalog;
  }

  @Override
  public List<Place> getAllPlaces() {
    if (catalog.getCatalog().isEmpty()) {
      throw new CatalogIsEmptyException("Sorry. No ads in catalog at this moment.");
    }
    return catalog.getCatalog();
  }

  @Override
  public List<Place> getPlacesCheaperThan(double price) throws NoAdsWithSuchCriteriaException {
    List<Place> result = new ArrayList<>();
    for (Place p : getAllPlaces()) {
      if (p.getRentPrice() <= price) {
        result.add(p);
      }
    }
    if (result.isEmpty()) {
      throw new NoAdsWithSuchCriteriaException("No ads cheaper than " + price);
    }
    return result;
  }

  @Override
  public List<Place> getPlacesWithDistanceToKindergarten(double distance)
      throws NoAdsWithSuchCriteriaException {
    List<Place> result = new ArrayList<>();
    for (Place p : getAllPlaces()) {
      if (p.getDistanceToKindergarten() <= distance) {
        result.add(p);
      }
    }
    if (result.isEmpty()) {
      throw new NoAdsWithSuchCriteriaException(
          "No ads with distance to kindergarted less than " + distance);
    }
    return result;
  }

  @Override
  public List<Place> getPlacesWithDistanceToSchool(double distance)
      throws NoAdsWithSuchCriteriaException {
    List<Place> result = new ArrayList<>();
    for (Place p : getAllPlaces()) {
      if (p.getDistanceToSchool() <= distance) {
        result.add(p);
      }
    }
    if (result.isEmpty()) {
      throw new NoAdsWithSuchCriteriaException(
          "No ads with distance to school less than " + distance);
    }
    return result;
  }

  @Override
  public List<Place> getPlacesWithDistanceToPlayground(double distance)
      throws NoAdsWithSuchCriteriaException {
    List<Place> result = new ArrayList<>();
    for (Place p : getAllPlaces()) {
      if (p.getDistanceToSchool() <= distance) {
        result.add(p);
      }
    }
    if (result.isEmpty()) {
      throw new NoAdsWithSuchCriteriaException(
          "No ads with distance to school less than " + distance);
    }
    return result;
  }

  @Override
  public List<Place> getPlacesWithLivingAreaBiggerThan(double livingArea)
      throws NoAdsWithSuchCriteriaException {
    List<Place> result = new ArrayList<>();
    for (Place p : getAllPlaces()) {
      if (p.getLivingArea() >= livingArea) {
        result.add(p);
      }
    }
    if (result.isEmpty()) {
      throw new NoAdsWithSuchCriteriaException(
          "No ads with living area bigger than " + livingArea);
    }
    return result;
  }

  @Override
  public void writeAdsToFile() {

  }
}