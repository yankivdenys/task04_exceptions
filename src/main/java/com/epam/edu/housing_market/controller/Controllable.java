package com.epam.edu.housing_market.controller;

import com.epam.edu.housing_market.model.Place;
import java.util.List;

interface Controllable {

  List<Place> getAllPlaces();

  List<Place> getPlacesCheaperThan(double price) throws NoAdsWithSuchCriteriaException;

  List<Place> getPlacesWithDistanceToKindergarten(double distance)
      throws NoAdsWithSuchCriteriaException;

  List<Place> getPlacesWithDistanceToSchool(double distance) throws NoAdsWithSuchCriteriaException;

  List<Place> getPlacesWithDistanceToPlayground(double distance)
      throws NoAdsWithSuchCriteriaException;

  List<Place> getPlacesWithLivingAreaBiggerThan(double livingArea)
      throws NoAdsWithSuchCriteriaException;

  void writeAdsToFile();
}