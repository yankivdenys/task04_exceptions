package com.epam.edu.housing_market.controller;

import com.epam.edu.housing_market.model.Place;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class AdsWriter implements AutoCloseable {
  public AdsWriter(){}

  public void writeAds(List<Place> placeList, String filename) {
    try(PrintWriter printWriter = new PrintWriter(filename)){
      for(Place p: placeList){
        printWriter.println(p.toString());
        //printWriter.write(p.toString());
      }
    } catch (IOException e) {
      System.out.println("Something is not right...");
    }
  }

  @Override
  public void close() throws Exception {
    System.out.println("Closing AdsWriter");
    throw new NullPointerException();
  }
}
