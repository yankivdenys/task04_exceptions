package com.epam.edu.housing_market.controller;

public class CatalogIsEmptyException extends RuntimeException {

  public CatalogIsEmptyException(String message) {
    super(message);
  }
}
