package com.epam.edu.housing_market.controller;

public class NoAdsWithSuchCriteriaException extends Exception {

  public NoAdsWithSuchCriteriaException(String message) {
    super(message);
  }
}
