package com.epam.edu.housing_market.view;

@FunctionalInterface
interface Printable{
    void print();
} 