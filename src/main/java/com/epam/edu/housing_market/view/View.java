package com.epam.edu.housing_market.view;

import com.epam.edu.housing_market.controller.AdsWriter;
import com.epam.edu.housing_market.controller.CatalogIsEmptyException;
import com.epam.edu.housing_market.controller.NoAdsWithSuchCriteriaException;
import com.epam.edu.housing_market.model.Catalog;
import com.epam.edu.housing_market.model.Place;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.epam.edu.housing_market.controller.Controller;

public final class View {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in, "UTF-8");

  public View() {
    controller = new Controller(new Catalog());
    menu = new LinkedHashMap<>();
    menu.put("1", "1 - show all ads");
    menu.put("2", "2 - filter ads by price");
    menu.put("3", "3 - filter ads by distance to kindergarten");
    menu.put("4", "4 - filter ads by distance to school");
    menu.put("5", "5 - filter ads by distance to playground");
    menu.put("6", "6 - write ads to file");
    menu.put("Q", "Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);
    methodsMenu.put("5", this::pressButton5);
    methodsMenu.put("6", this::pressButton6);
  }

  private <E> void printList(final List<E> list) {
    for (E o : list) {
      System.out.println(o);
    }
  }

  private void pressButton1() {
    List<Place> list;
    try {
      list = controller.getAllPlaces();
      printList(list);
    } catch (CatalogIsEmptyException e) {
      System.out.println(e.getMessage());
    }
  }

  private void pressButton2() {
    System.out.println("Enter rent upper price: ");
    double price = input.nextDouble();
    List<Place> list;
    try {
      list = controller.getPlacesCheaperThan(price);
      printList(list);
    } catch (NoAdsWithSuchCriteriaException e) {
      System.out.println(e.getMessage());
    }
  }

  private void pressButton3() {
    System.out.println("Enter max distance to kindergarten: ");
    double distance = input.nextDouble();
    List<Place> list;
    try {
      list = controller.getPlacesWithDistanceToKindergarten(distance);
      printList(list);
    } catch (NoAdsWithSuchCriteriaException e) {
      System.out.println(e.getMessage());
    }
  }

  private void pressButton4() {
    System.out.println("Enter max distance to school: ");
    double distance = input.nextDouble();
    List<Place> list;
    try {
      list = controller.getPlacesWithDistanceToSchool(distance);
      printList(list);
    } catch (NoAdsWithSuchCriteriaException e) {
      System.out.println(e.getMessage());
    }
  }

  private void pressButton5() {
    System.out.println("Enter max distance to playground: ");
    double distance = input.nextDouble();
    List<Place> list;
    try {
      list = controller.getPlacesWithDistanceToPlayground(distance);
      printList(list);
    } catch (NoAdsWithSuchCriteriaException e) {
      System.out.println(e.getMessage());
    }
  }

  private void pressButton6() {
    System.out.println("Enter filename: ");
    String fileName = input.nextLine();
    new AdsWriter().writeAds(controller.getAllPlaces(),fileName);
  }

  private void outputMenu() {
    System.out.println("\nMenu:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toLowerCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
        System.out.println("Wrong input.");
      }
    } while (!keyMenu.equalsIgnoreCase("q"));
  }
}