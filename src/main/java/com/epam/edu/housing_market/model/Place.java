package com.epam.edu.housing_market.model;

public class Place {

  private double livingArea;
  private String address;
  private double distanceToKindergarten;
  private double distanceToSchool;
  private double distanceToPlayground;
  private double rentPrice;

  public double getLivingArea() {
    return livingArea;
  }

  public void setLivingArea(double livingArea) {
    if (livingArea <= 0){
      throw new IllegalArgumentException("Living area must be > 0");
    }
    this.livingArea = livingArea;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public double getDistanceToKindergarten() {
    return distanceToKindergarten;
  }

  public void setDistanceToKindergarten(double distanceToKindergarten) {
    this.distanceToKindergarten = distanceToKindergarten;
  }

  public double getDistanceToSchool() {
    return distanceToSchool;
  }

  public void setDistanceToSchool(double distanceToSchool) {
    this.distanceToSchool = distanceToSchool;
  }

  public double getDistanceToPlayground() {
    return distanceToPlayground;
  }

  public void setDistanceToPlayground(double distanceToPlayground) {
    this.distanceToPlayground = distanceToPlayground;
  }

  public double getRentPrice() {
    return rentPrice;
  }

  public void setRentPrice(double rentPrice) {
    this.rentPrice = rentPrice;
  }

  public Place(double livingArea, String address, double distanceToKindergarten, double distanceToSchool,
      double distanceToPlayground, double rentPrice) {
    this.livingArea = livingArea;
    this.address = address;
    this.distanceToKindergarten = distanceToKindergarten;
    this.distanceToSchool = distanceToSchool;
    this.distanceToPlayground = distanceToPlayground;
    this.rentPrice = rentPrice;
  }

  @Override
  public String toString() {
    return "Place [address=" + address + ", distanceToKindergarten=" + distanceToKindergarten
        + ", distanceToPlayground=" + distanceToPlayground + ", distanceToSchool=" + distanceToSchool + ", livingArea="
        + livingArea + ", rentPrice=" + rentPrice + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((address == null) ? 0 : address.hashCode());
    long temp;
    temp = Double.doubleToLongBits(distanceToKindergarten);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(distanceToPlayground);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(distanceToSchool);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(livingArea);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(rentPrice);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Place other = (Place) obj;
    if (address == null) {
      if (other.address != null)
        return false;
    } else if (!address.equals(other.address))
      return false;
    if (Double.doubleToLongBits(distanceToKindergarten) != Double.doubleToLongBits(other.distanceToKindergarten))
      return false;
    if (Double.doubleToLongBits(distanceToPlayground) != Double.doubleToLongBits(other.distanceToPlayground))
      return false;
    if (Double.doubleToLongBits(distanceToSchool) != Double.doubleToLongBits(other.distanceToSchool))
      return false;
    if (Double.doubleToLongBits(livingArea) != Double.doubleToLongBits(other.livingArea))
      return false;
    if (Double.doubleToLongBits(rentPrice) != Double.doubleToLongBits(other.rentPrice))
      return false;
    return true;
  }

  
}
