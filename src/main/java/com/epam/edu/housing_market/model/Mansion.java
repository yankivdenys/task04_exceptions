package com.epam.edu.housing_market.model;

class Mansion extends Place {

    private int numberOfFloors;
    private double outdoorsArea;

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    public double getOutdoorsArea() {
        return outdoorsArea;
    }

    public void setOutdoorsArea(double outdoorsArea) {
        this.outdoorsArea = outdoorsArea;
    }

    public Mansion(double livingArea, String address, double distanceToKindergarten, double distanceToSchool,
            double distanceToPlayground, double rentPrice, int numberOfFloors, double outdoorsArea) {
        super(livingArea, address, distanceToKindergarten, distanceToSchool, distanceToPlayground, rentPrice);
        this.numberOfFloors = numberOfFloors;
        this.outdoorsArea = outdoorsArea;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + numberOfFloors;
        long temp;
        temp = Double.doubleToLongBits(outdoorsArea);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Mansion other = (Mansion) obj;
        if (numberOfFloors != other.numberOfFloors)
            return false;
        if (Double.doubleToLongBits(outdoorsArea) != Double.doubleToLongBits(other.outdoorsArea))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return super.toString() + " Mansion [numberOfFloors=" + numberOfFloors + ", outdoorsArea=" + outdoorsArea + "]";
    }

    
}