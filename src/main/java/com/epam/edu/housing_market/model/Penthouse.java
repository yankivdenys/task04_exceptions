package com.epam.edu.housing_market.model;

class Penthouse extends Place {

  private int numberOfFloors;
  private boolean terrace;

  public int getNumberOfFloors() {
    return numberOfFloors;
  }

  public void setNumberOfFloors(int numberOfFloors) {
    this.numberOfFloors = numberOfFloors;
  }

  public boolean isTerrace() {
    return terrace;
  }

  public void setTerrace(boolean terrace) {
    this.terrace = terrace;
  }

  public Penthouse(double livingArea, String address, double distanceToKindergarten, double distanceToSchool,
      double distanceToPlayground, double rentPrice, int numberOfFloors, boolean terrace) {
    super(livingArea, address, distanceToKindergarten, distanceToSchool, distanceToPlayground, rentPrice);
    this.numberOfFloors = numberOfFloors;
    this.terrace = terrace;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + numberOfFloors;
    result = prime * result + (terrace ? 1231 : 1237);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    Penthouse other = (Penthouse) obj;
    if (numberOfFloors != other.numberOfFloors)
      return false;
    if (terrace != other.terrace)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return super.toString() + " Penthouse [numberOfFloors=" + numberOfFloors + ", terrace=" + terrace + "]";
  }


  
}