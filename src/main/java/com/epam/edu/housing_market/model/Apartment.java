package com.epam.edu.housing_market.model;

class Apartment extends Place {

  private int numberOfRooms;

  public int getNumberOfRooms() {
    return numberOfRooms;
  }

  public void setNumberOfRooms(int numberOfRooms) {
    this.numberOfRooms = numberOfRooms;
  }

  public Apartment(double livingArea, String address, double distanceToKindergarten, double distanceToSchool,
      double distanceToPlayground, double rentPrice, int numberOfRooms) {
    super(livingArea, address, distanceToKindergarten, distanceToSchool, distanceToPlayground, rentPrice);
    this.numberOfRooms = numberOfRooms;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + numberOfRooms;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    Apartment other = (Apartment) obj;
    if (numberOfRooms != other.numberOfRooms)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return super.toString() + " Apartment [numberOfRooms=" + numberOfRooms + "]";
  }

  
  
  

}