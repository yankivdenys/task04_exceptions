package com.epam.edu.housing_market.model;

import java.util.ArrayList;
import java.util.List;

public final class Catalog {

  public Catalog() {
  }

  public  List<Place> getCatalog() {
    List<Place> catalogList = new ArrayList<>();

    Mansion mansion1 = new Mansion(120, "lviv1", 122, 130, 130, 50, 2, 40);
    Mansion mansion2 = new Mansion(300, "lviv2", 122, 130, 130, 100, 2, 160);

    Apartment apartment1 = new Apartment(65, "lviv3", 40, 50, 40, 30, 1);
    Apartment apartment2 = new Apartment(80, "lviv4", 60, 60, 200, 60, 3);

    Penthouse penthouse1 = new Penthouse(200, "lviv5", 40, 30, 30, 100, 1, true);
    Penthouse penthouse2 = new Penthouse(200, "lviv6", 40, 30, 30, 100, 1, false);

    catalogList.add(mansion1);
    catalogList.add(mansion2);
    catalogList.add(apartment1);
    catalogList.add(apartment2);
    catalogList.add(penthouse1);
    catalogList.add(penthouse2);

    return catalogList;
  }
}
